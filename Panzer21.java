package A;
import robocode.*;
import java.awt.Color;

public class Panzer21 extends AdvancedRobot
{
	public void run() 
	{
	//Robot Colors
	setBodyColor(Color.black);
	setGunColor(Color.black);
	setRadarColor(Color.black);
	setScanColor(Color.red);
	setBulletColor(Color.red);
	
		/*
		Raciocínio:
		Movimento retilíneo, lento e com angulação suave do scanner/canhão para maior probabilidade de acertar os tiros.
		Movimento lento, porém não tanto a ponto de ser atingido por tiros.
		Modo de economia de energia: Qaunto menos energia, mais fraco o tiro.
		*/
		while(true) 
		{
		setAhead(10);
		setTurnGunLeft(10);
		execute();
		}
	}
	
		public void onScannedRobot(ScannedRobotEvent e) 
		{			
			if (e.getEnergy() > 15)
			{fire(3);}
		    else if (e.getEnergy() > 10)
			{fire(2);}
		 	else if (e.getEnergy() > 0)
			{fire(1);}
		} 
	
		public void onHitWall(HitWallEvent e) 
		{
		setTurnRight(90);
		}
}	
